use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;

mod camera;
mod car;

use camera::*;
use car::*;


fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let vid_subsys = sdl_context.video()?;

    let win = vid_subsys
        .window("Win", 640, 480)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = win
        .into_canvas()
        .build()
        .map_err(|e| e.to_string())?;

    let main_cam = Camera {
        pos: glam::Vec2::ZERO, rot: 0.0, zoom: 16.0
    };
    let mut basic_car = Car {
        pos: glam::Vec2::ZERO, vel: glam::Vec2::ZERO,
        rot: 0.0, rot_vel: 0.0
    };
    let mut car_input = CarInputs { forward: false, left: false, right: false };

    let instant = std::time::Instant::now();
    let mut last_time = instant.elapsed();
    let mut time_bank: f32 = 0.0;

    let fix_dt = 1.0 / 100.0;
    let mut dt = 0.0;

    let mut evt_pump = sdl_context.event_pump()?;
    let mut running = false;

    while !running {
        for evt in evt_pump.poll_iter() {
            match evt {
                Event::Quit { .. } => {
                    running = true;
                },
                Event::KeyDown { keycode, repeat, .. } => {
                    if keycode.is_some() && !repeat {
                        match keycode.unwrap() {
                            Keycode::Z => car_input.forward = true,
                            Keycode::Q => car_input.left = true,
                            Keycode::D => car_input.right = true,
                            _ => { }
                        }
                    }
                },
                Event::KeyUp { keycode, repeat, .. } => {
                    if keycode.is_some() && !repeat {
                        match keycode.unwrap() {
                            Keycode::Z => car_input.forward = false,
                            Keycode::Q => car_input.left = false,
                            Keycode::D => car_input.right = false,
                            _ => { }
                        }
                    }
                },
                _ => {}
            }
        }

        while time_bank > fix_dt {
            basic_car.fix_update(fix_dt);
            time_bank -= fix_dt;
        }

        basic_car.update(dt, &car_input);

        canvas.set_draw_color(Color::RGB(50, 50, 50));
        canvas.clear();
        basic_car.render(&main_cam, &mut canvas);
        canvas.present();

        let new_time = instant.elapsed();
        dt = new_time.as_secs_f32() - last_time.as_secs_f32();
        time_bank += dt;
        last_time = instant.elapsed();

        if time_bank > 0.2 {
            time_bank = 0.2;
        }
    }

    Ok(())
}