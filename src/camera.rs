pub struct Camera {
    pub pos: glam::Vec2,
    pub rot: f32,
    pub zoom: f32,
    
}

impl Camera {
    pub fn to_local(self: &Self, world_pos: glam::Vec2) -> glam::Vec2 {
        glam::Vec2::from_angle(self.rot)
            .rotate(world_pos - self.pos) * self.zoom
    }

}