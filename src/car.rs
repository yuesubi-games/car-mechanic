use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::render::Canvas;
use sdl2::video::Window;

use crate::camera::Camera;


const CAR_W: f32 = 1.75;
const CAR_L: f32 = 4.55;

const CAR_VERTICES: [glam::Vec2; 4] = [
    glam::vec2(CAR_W / 2.0, -CAR_L / 2.0),
    glam::vec2(-CAR_W / 2.0, -CAR_L / 2.0),
    glam::vec2(-CAR_W / 2.0, CAR_L / 2.0),
    glam::vec2(CAR_W / 2.0, CAR_L / 2.0),
];

const CAR_LINES: [(usize, usize); 4] = [ (0, 1), (1, 2), (2, 3), (3, 0) ];

const WHEEL_AMOUNT: usize = 4;
const WHEEL_NORMAL: glam::Vec2 = glam::vec2(1.0, 0.0);

const WHEEL_W: f32 = 0.25;
const WHEEL_RADIUS: f32 = 0.33;

const WHEEL_OFFSETS: [glam::Vec2; WHEEL_AMOUNT] = [
    glam::vec2((CAR_W - WHEEL_W) / 2.0, 1.08),
    glam::vec2(-(CAR_W - WHEEL_W) / 2.0, 1.08),
    glam::vec2((CAR_W - WHEEL_W) / 2.0, -1.51),
    glam::vec2(-(CAR_W - WHEEL_W) / 2.0, -1.51),
];


pub struct CarInputs {
    pub forward: bool,
    pub left: bool,
    pub right: bool,
}


pub struct Wheel {
    pub offset: glam::Vec2,
    pub rot: f32,
    pub speed: f32
}

impl Wheel {
    pub fn from_offset(offset: glam::Vec2) -> Self {
        Wheel {
            offset, rot: 0.0,
            speed: 0.0
        }
    }

    pub fn render(
            self: &Self,
            pos: glam::Vec2,
            rot: f32,
            cam: &Camera,
            canvas: &mut Canvas<Window>
    ) {
        let vertices = [
            glam::vec2(WHEEL_W, WHEEL_RADIUS) / 2.0,
            glam::vec2(-WHEEL_W, WHEEL_RADIUS) / 2.0,
            glam::vec2(WHEEL_W, -WHEEL_RADIUS) / 2.0,
            glam::vec2(-WHEEL_W, -WHEEL_RADIUS) / 2.0,
        ];

        canvas.set_draw_color(Color::RGB(255, 0, 0);

        for i in 0..WHEEL_AMOUNT {

        }
    }
}


pub struct Car {
    pub pos: glam::Vec2,
    pub vel: glam::Vec2,

    pub rot: f32,
    pub rot_vel: f32,

    pub wheels: [Wheel; 4]
}

impl Car {
    pub fn from_pos_and_rot(pos: glam::Vec2, rot: f32) -> Self {
        Car {
            pos, vel: glam::Vec2::ZERO,
            rot, rot_vel: 0.0,
            wheels: [
                Wheel::from_offset(WHEEL_OFFSETS[0]),
                Wheel::from_offset(WHEEL_OFFSETS[1]),
                Wheel::from_offset(WHEEL_OFFSETS[2]),
                Wheel::from_offset(WHEEL_OFFSETS[3]),
            ]
        }
    }

    pub fn fix_update(self: &mut Self, fix_dt: f32) {
        self.vel *= 1.0 - fix_dt;
        self.rot_vel *= 1.0 - fix_dt;

        self.pos += self.vel * fix_dt * 5.0;
        self.rot += self.rot_vel * fix_dt;
    }

    pub fn update(self: &mut Self, dt: f32, input: &CarInputs) {
        //self.vel = glam::Vec2::ZERO;

        if input.forward {
            self.vel = self.vel.lerp(
                glam::Vec2::from_angle(self.rot).rotate(glam::Vec2::Y),
                dt * 5.0
            );
        }

        if input.left {
            self.rot_vel -= 2.0 * dt;
        }
        if input.right {
            self.rot_vel += 2.0 * dt;
        }
    }
    
    pub fn render(
            self: &Self,
            cam: &Camera,
            canvas: &mut Canvas<Window>
    ) {
        //let rect = Rect::new(32, 32, 128, 128);
        canvas.set_draw_color(Color::RGB(255, 255, 255));

        let canv_size = canvas.output_size().unwrap();
        let middle = glam::vec2(canv_size.0 as f32, canv_size.1 as f32) / 2.0;
        let rot_vec = glam::Vec2::from_angle(self.rot);

        for (start_id, end_id) in CAR_LINES {
            let start = cam.to_local(rot_vec
                .rotate(CAR_VERTICES[start_id]) + self.pos) + middle;
            let end = cam.to_local(rot_vec
                .rotate(CAR_VERTICES[end_id]) + self.pos) + middle;

            let start_p = Point::from((start.x as i32, start.y as i32));
            let end_p = Point::from((end.x as i32, end.y as i32));
            canvas.draw_line(start_p, end_p).unwrap();
        }
    }
}